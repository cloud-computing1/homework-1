import { AxiosError } from 'axios'

export const APIErrorType = {
  auth: { status: 401, data: { message: 'Not signed in' } },
  notFound: { status: 404, data: { message: 'Not found' } },
  internal: { status: 500, data: { message: 'Internal server error' } },
  gateway: { status: 502, data: { message: 'Bad gateway' } },
  401: { status: 401, data: { message: 'Not signed in' } },
  404: { status: 404, data: { message: 'Not found' } },
  500: { status: 500, data: { message: 'Internal server error' } },
  502: { status: 502, data: { message: 'Bad gateway' } },
}

export class APIError extends Error {
  error: Error
  status: number
  data: Record<string, unknown>

  constructor (error: Error) {
    super(error.message)
    this.error = error;
    this.status = this.getErrorType(error)
    this.data = APIErrorType[this.status as keyof typeof APIErrorType].data
  }

  getErrorType(err: Error): number {
    if (isAxiosError(err)) {
      if (err.response?.status) {
        return Number(err.response.status)
      } else if (err.code === 'ECONNREFUSED') {
        return 502
      }
    }

    return 500
  }

  toString() {
    return `API error: '${this.status}' error at ${this.error.stack}`
  }
}

export const isAPIError = (err: Error | APIError | AxiosError): err is APIError =>
  Object.keys(err).includes('status') &&
  Object.keys(err).includes('data')
export const isAxiosError = (err: Error | APIError | AxiosError): err is AxiosError =>
  Object.keys(err).includes('response') ||
  Object.keys(err).includes('code') ||
  Object.keys(err).includes('errno')
