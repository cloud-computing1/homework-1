import { Singleton, SingletonContainer } from '../types'

export function create <TSingleton extends Singleton> (
  container: SingletonContainer<TSingleton>,
  constructor: () => TSingleton
): TSingleton {
  if (container.current === null) {
    container.current = constructor()
  }

  return container.current
}
