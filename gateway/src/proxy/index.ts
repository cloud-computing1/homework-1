export * as authProxy from './auth'
export * as permissionsProxy from './permissions'
export * as articlesProxy from './articles'
