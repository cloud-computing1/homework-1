import axios from 'axios'
import { constants } from '@config'
import { singleton } from '@utils'
import { PublicArticle, Singleton, SingletonContainer } from '@types'

export interface ArticlesProxy extends Singleton {
  get: (token: string, containing: string, skip: number, take: number) => Promise<PublicArticle[]>
  create: (token: string, article: string) => Promise<PublicArticle>
}

const container: SingletonContainer<ArticlesProxy> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): ArticlesProxy {
  const client = axios.create({
    baseURL: constants.articlesBaseURL
  })

  return {
    get,
    create
  }

  async function get (token: string, containing: string, skip: number, take: number): Promise<PublicArticle[]> {
    const response = await client.get<PublicArticle[]>(`/api/articles`, {
      headers: { Authorization: `Bearer ${token}` },
      params: { containing, skip, take }
    })
    return response.data
  }

  async function create (token: string, article: string): Promise<PublicArticle> {
    const response = await client.post<PublicArticle>(
      `/api/articles`,
      { article },
      {
        headers: { Authorization: `Bearer ${token}` },
      }
    )
    return response.data
  }
}
