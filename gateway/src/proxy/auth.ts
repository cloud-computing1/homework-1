import axios from 'axios'
import { constants } from '@config'
import { singleton } from '@utils'
import { PublicCSRFToken, PublicCSRFTokenValidity, PublicSession, PublicUser, Singleton, SingletonContainer } from '@types'

export interface AuthProxy extends Singleton {
  createSession: (email: string, password: string) => Promise<PublicSession>
  getSessionByToken: (token: string) => Promise<PublicSession>
  createUser: (email: string, password: string) => Promise<PublicUser>
  getUserById: (token: string, userId: string) => Promise<PublicUser>
  getCSRFToken: () => Promise<PublicCSRFToken>
  checkCSRFToken: (token: string) => Promise<PublicCSRFTokenValidity>
}

const container: SingletonContainer<AuthProxy> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): AuthProxy {
  const client = axios.create({
    baseURL: constants.authBaseURL
  })

  return {
    createSession,
    getSessionByToken,
    createUser,
    getUserById,
    getCSRFToken,
    checkCSRFToken
  }

  async function createSession (email: string, password: string): Promise<PublicSession> {
    const response = await client.post<PublicSession>(`/api/sessions`, { email, password })
    return response.data
  }

  async function getSessionByToken (token: string): Promise<PublicSession> {
    const response = await client.get<PublicSession>(`/api/sessions/${token}`)
    return response.data
  }

  async function createUser (email: string, password: string): Promise<PublicUser> {
    const response = await client.post<PublicUser>(`/api/users`, { email, password })
    return response.data
  }

  async function getUserById (token: string, userId: string): Promise<PublicUser> {
    const response = await client.get<PublicUser>(`/api/users/${userId}`, {
      headers: { Authorization: `Bearer ${token}` }
    })
    return response.data
  }

  async function getCSRFToken (): Promise<PublicCSRFToken> {
    const response = await client.get<PublicCSRFToken>(`/api/csrf`)
    return response.data
  }

  async function checkCSRFToken (token: string): Promise<PublicCSRFTokenValidity> {
    const response = await client.get<PublicCSRFTokenValidity>(`/api/csrf/${token}`)
    return response.data
  }
}
