import axios from 'axios'
import { constants } from '@config'
import { singleton } from '@utils'
import { PublicPermissions, Singleton, SingletonContainer } from '@types'

export interface PermissionsProxy extends Singleton {
  getByUserId: (token: string, userId: string) => Promise<PublicPermissions>
}

const container: SingletonContainer<PermissionsProxy> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): PermissionsProxy {
  const client = axios.create({
    baseURL: constants.permissionsBaseURL
  })

  return {
    getByUserId,
  }

  async function getByUserId (token: string, userId: string): Promise<PublicPermissions> {
    const response = await client.get<PublicPermissions>(`/api/users/${userId}/permissions`, {
      headers: { Authorization: `Bearer ${token}` }
    })
    return response.data
  }
}
