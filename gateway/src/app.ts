import express from 'express'
import { proc, proxy, server, services } from '@config'
import { articlesRouter, sessionsRouter, usersRouter, permissionsRouter, viewsRouter, metricsRouter } from '@routes'
import { handleError } from '@middleware'

export async function app (): Promise<express.Application> {
  const app = express()

  proc.init()
  proxy.init()
  services.init()
  server.init(app)

  app.use('/api/metrics', metricsRouter)
  app.use('/api/users', usersRouter)
  app.use('/api/users/:userId/permissions', permissionsRouter)
  app.use('/api/sessions', sessionsRouter)
  app.use('/api/articles', articlesRouter)
  app.use(viewsRouter)
  app.use(handleError)

  return app
}
