import { Metrics, HTTPMetric, Singleton, SingletonContainer, CSRFMetrics, FileMetrics } from '@types'
import { singleton } from '@utils'

export interface MetricsService extends Singleton {
  get: () => Metrics
  addHTTPMetric: (metric: HTTPMetric) => void
  setCSRFMetric: (metric: CSRFMetrics) => void
  setFilesMetric: (metric: FileMetrics) => void
}

const container: SingletonContainer<MetricsService> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): MetricsService {
  const metrics: Metrics = {
    http: [],
    csrf: { tokens: 0, successfulRequests: 0, failedRequests: 0 },
    files: { cached: {} },
    users: { count: 0 }
  }

  return {
    get,
    addHTTPMetric,
    setCSRFMetric,
    setFilesMetric
  }

  function get (): Metrics {
    return metrics
  }

  function addHTTPMetric (metric: HTTPMetric) {
    metrics.http.push(metric)
  }

  function setCSRFMetric (metric: CSRFMetrics) {
    metrics.csrf = metric
  }

  function setFilesMetric (metric: FileMetrics) {
    metrics.files = metric
  }
}
