import { AuthProxy } from '@proxy/auth'
import { APIError, singleton } from '@utils'
import { PublicSession, Singleton, SingletonContainer } from '@types'

export interface SessionsService extends Singleton {
  getByToken: (token: string) => Promise<PublicSession> 
  create: (email: string, password: string) => Promise<PublicSession> 
}

const container: SingletonContainer<SessionsService> = { current: null }

export const getInstance = (
  authProxy?: AuthProxy
) => singleton.create(container, () => {
  if (!authProxy) {
    throw new Error('[Sessions service] Init error')
  } else {
    return constructor(authProxy)
  }
})

function constructor (
  authProxy: AuthProxy
): SessionsService {
  return {
    getByToken,
    create
  }

  async function getByToken (token: string): Promise<PublicSession> {
    try {
      const session = await authProxy.getSessionByToken(token)
      return session
    } catch (err) {
      throw new APIError(err)
    }
  }

  async function create (email: string, password: string): Promise<PublicSession> {
    try {
      const session = await authProxy.createSession(email, password)
      return session
    } catch (err) {
      throw new APIError(err)
    }
  }
}
