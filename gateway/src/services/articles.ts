import { PublicArticle, Singleton, SingletonContainer } from '@types'
import { APIError, singleton } from '@utils'
import { ArticlesProxy } from '@proxy/articles'

export interface ArticlesService extends Singleton {
  get: (token: string, containing: string, skip: number, take: number) => Promise<PublicArticle[]>
}

const container: SingletonContainer<ArticlesService> = { current: null }

export const getInstance = (
  articlesProxy?: ArticlesProxy
) => singleton.create(container, () => {
  if (!articlesProxy) {
    throw new Error('[Articles Service] Init error')
  } else {
    return constructor(articlesProxy)
  }
})

function constructor (
  articlesProxy: ArticlesProxy
): ArticlesService {
  return {
    get
  }

  async function get (token: string, containing: string, skip: number, take: number): Promise<PublicArticle[]> {
    try {
      const articles = await articlesProxy.get(token, containing, skip, take)
      return articles
    } catch (err) {
      throw new APIError(err)
    }
  }
}
