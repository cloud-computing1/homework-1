import { AuthProxy } from '@proxy/auth'
import { APIError, singleton } from '@utils'
import { PublicUser, Singleton, SingletonContainer } from '@types'

export interface UsersService extends Singleton {
  create: (email: string, password: string) => Promise<PublicUser>
  getByUserId: (token: string, userId: string) => Promise<PublicUser>
}

const container: SingletonContainer<UsersService> = { current: null }

export const getInstance = (
  authProxy?: AuthProxy
) => singleton.create(container, () => {
  if (!authProxy) {
    throw new Error('[Users service] Init error')
  } else {
    return constructor(authProxy)
  }
})

function constructor (
  authProxy: AuthProxy
): UsersService {
  return {
    create,
    getByUserId
  }

  async function create (email: string, password: string): Promise<PublicUser> {
    try {
      const user = await authProxy.createUser(email, password)
      return user
    } catch (err) {
      throw new APIError(err)
    }
  }

  async function getByUserId (token: string, userId: string): Promise<PublicUser> {
    try {
      const user = await authProxy.getUserById(token, userId)
      return user
    } catch (err) {
      throw new APIError(err)
    }
  }
}
