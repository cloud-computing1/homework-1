import { PermissionsProxy } from '@proxy/permissions'
import { APIError, singleton } from '@utils'
import { PublicPermissions, Singleton, SingletonContainer } from '@types'

export interface PermissionsService extends Singleton {
  getByUserId: (token: string, userId: string) => Promise<PublicPermissions> 
}

const container: SingletonContainer<PermissionsService> = { current: null }

export const getInstance = (
  permissionsProxy?: PermissionsProxy
) => singleton.create(container, () => {
  if (!permissionsProxy) {
    throw new Error('[Permissions service] Init error')
  } else {
    return constructor(permissionsProxy)
  }
})

function constructor (
  permissionsProxy: PermissionsProxy
): PermissionsService {
  return {
    getByUserId,
  }

  async function getByUserId (email: string, password: string): Promise<PublicPermissions> {
    try {
      const token = await permissionsProxy.getByUserId(email, password)
      return token
    } catch (err) {
      throw new APIError(err)
    }
  }
}
