import { AuthProxy } from '@proxy/auth'
import { MetricsService } from '@services/metrics'
import { CSRFMetrics, PublicCSRFToken, PublicCSRFTokenValidity, Singleton, SingletonContainer } from '@types'
import { APIError, singleton } from '@utils'

export interface CSRFService extends Singleton {
  get: () => Promise<PublicCSRFToken>
  check: (token: string) => Promise<PublicCSRFTokenValidity>
}

const container: SingletonContainer<CSRFService> = { current: null }

export const getInstance = (
  authProxy?: AuthProxy,
  metricsService?: MetricsService
) => singleton.create(container, () => {
  if (!authProxy || !metricsService) {
    throw new Error('[CSRF Service] Init error')
  } else {
    return constructor(authProxy, metricsService)
  }
})

function constructor (
  authProxy: AuthProxy,
  metricsService: MetricsService
): CSRFService {
  const metrics: CSRFMetrics = { tokens: 0, successfulRequests: 0, failedRequests: 0 }

  return {
    get,
    check
  }

  async function get (): Promise<PublicCSRFToken> {
    try {
      const token = await authProxy.getCSRFToken()

      metrics.tokens++

      updateMetrics()

      return token
    } catch (err) {
      throw new APIError(err)
    }
  }

  async function check (token: string): Promise<PublicCSRFTokenValidity> {
    try {
      const validity = await authProxy.checkCSRFToken(token)

      updateMetrics()

      return validity
    } catch (err) {
      throw new APIError(err)
    }
  }

  function updateMetrics () {
    metricsService.setCSRFMetric(metrics)
  }
}
