import fs from 'fs/promises'
import { constants } from '@config'
import { FileMetrics, Singleton, SingletonContainer } from '@types'
import { singleton } from '@utils'
import { MetricsService } from './metrics'

export interface ViewsService extends Singleton {
  getFile: (path: string) => Promise<string>
}

const container: SingletonContainer<ViewsService> = { current: null }

export const getInstance = (
  metricsService?: MetricsService
) => singleton.create(container, () => {
  if (!metricsService) {
    throw new Error('[Files Service] Init error')
  } else {
    return constructor(metricsService)
  }
})

function constructor (
  metricsService: MetricsService
): ViewsService {
  const shouldCache = constants.ShouldCache
  const metrics: FileMetrics = { cached: {} }
  const cache: Record<string, string> = {}

  return {
    getFile
  }

  async function getFile (path: string): Promise<string> {
    if (shouldCache) {
      if (typeof cache[path] === 'string') {
        return cache[path] as string
      }
    }

    const fileBuffer = await fs.readFile(`${constants.PublicDir}${path}`)
    const fileString = fileBuffer.toString()

    if (shouldCache) {
      cache[path] = fileString
      metrics.cached = cache
      updateMetrics()
    }

    return fileString
  }

  function updateMetrics () {
    metricsService.setFilesMetric(metrics)
  }
}
