import express from 'express'
import { AxiosError } from 'axios'
import { APIError, APIErrorType, isAPIError, isAxiosError } from '@utils'

export function handleError (err: Error | APIError | AxiosError, _1: express.Request, res: express.Response, _2: express.NextFunction) {
  log(err)
  handle(err)

  function log (err: Error | APIError | AxiosError) {
    if (isAPIError(err)) {
      console.error(err.toString())
    } else if (isAxiosError(err)) {
      console.error(`Axios error: '${err.code}' error at ${err.stack}`)
    } else {
      console.error(`Generic error: '${err.message}' error at ${err.stack}`)
    }
  }

  function handle (err: Error | APIError | AxiosError) {
    if (isAPIError(err)) {
      res.status(err.status).send(err.data)
      return
    } else if (isAxiosError(err)) {
      if (err.response?.status) {
        const error = APIErrorType[err.response.status as keyof typeof APIErrorType]
        res.status(error.status).send(error.data)
        return
      }
    }

    res.status(APIErrorType.internal.status).send(APIErrorType.internal.data)
  }
}
