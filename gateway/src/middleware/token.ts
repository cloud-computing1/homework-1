import express from 'express';

export function get (req: express.Request, _: express.Response, next: express.NextFunction) {
  const token = Array.isArray(req.headers.authorization)
    ? req.headers.authorization[0]?.split(' ')[1] ?? ''
    : req.headers.authorization?.split(' ')[1] ?? ''

  req.token = token

  next()
}
