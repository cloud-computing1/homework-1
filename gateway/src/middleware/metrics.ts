import perfHooks from 'perf_hooks'
import express from 'express'
import { metricsService } from '@services'

export function measure (req: express.Request, res: express.Response, next: express.NextFunction): void {
  const path = req.path
  const params = req.params
  const thenDate = new Date()
  const thenMs = perfHooks.performance.now()

  res.on('finish', () => {
    const nowMs = perfHooks.performance.now()

    metricsService.getInstance().addHTTPMetric({
      startedAt: thenDate.toISOString(),
      endAt: new Date().toISOString(),
      durationMs: nowMs - thenMs,
      params,
      path
    })
  })

  next()
}
