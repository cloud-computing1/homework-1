import express from 'express'
import { permissionsService } from '@services'

export async function getByUserId (req: express.Request, res: express.Response) {
  const token = req.token || ''
  const userId = req.params.userId || ''

  const permissions = await permissionsService.getInstance().getByUserId(token, userId)

  res.status(200).send(permissions)
}
