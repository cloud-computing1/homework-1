import express from 'express'
import { sessionsService } from '@services'

export async function getByToken (req: express.Request, res: express.Response) {
  const token = req.token || ''

  const session = await sessionsService.getInstance().getByToken(token)

  res.status(200).send(session)
}

export async function create (req: express.Request, res: express.Response) {
  const email = req.body.email || ''
  const password = req.body.password || ''

  const session = await sessionsService.getInstance().create(email, password)

  res.status(200).send(session)
}
