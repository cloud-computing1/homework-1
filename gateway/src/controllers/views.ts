import express from 'express'
import { csrfService, filesService } from '@services'

export async function getIndexPage (_: express.Request, res: express.Response) {
  const csrfToken = await csrfService.getInstance().get()

  res.render('index', { csrfToken })
};

export async function getLoginPage (_: express.Request, res: express.Response) {
  const csrfToken = await csrfService.getInstance().get()

  res.render('login', { csrfToken })
};

export async function getFloodPage (_: express.Request, res: express.Response) {
  const csrfToken = await csrfService.getInstance().get()

  res.render('flood', { csrfToken })
};

export async function getMetricsPage (_: express.Request, res: express.Response) {
  const csrfToken = await csrfService.getInstance().get()

  res.render('metrics', { csrfToken })
};

export async function getFile (req: express.Request, res: express.Response) {
  try {
    const file = req.path
    const result = await filesService.getInstance().getFile(file)

    res.status(200).send(result)
  } catch (err) {
    res.status(404).send()
  }
};
