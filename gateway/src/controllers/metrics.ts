import express from 'express'
import { metricsService } from '@services'

export async function get (_: express.Request, res: express.Response) {
  const metrics = metricsService.getInstance().get()

  res.status(200).send(metrics)
}
