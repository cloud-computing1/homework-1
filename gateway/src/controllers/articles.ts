import express from 'express'
import { articlesService } from '@services'

export async function get (req: express.Request, res: express.Response) {
  const token = req.token || ''
  const containing = typeof req.query.containing === 'string' ? req.query.containing : ''
  const skip = Number(req.query.skip || 0)
  const take = !Number.isNaN(req.query.take) && Number(req.query.take) < 25
    ? Number(req.query.take)
    : 25

  const articles = await articlesService.getInstance().get(token, containing, skip, take)

  res.status(200).send(articles)
}
