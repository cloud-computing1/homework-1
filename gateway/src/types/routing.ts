import express from 'express';

export type AsyncMiddleware = (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction
) => Promise<void>
