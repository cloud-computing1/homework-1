export type PublicArticle = {
  id: string,
  userId: string
  article: string
}
