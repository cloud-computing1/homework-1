export type PublicUser = {
  userId: string,
  email: string
}
