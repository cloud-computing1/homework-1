import express from 'express'
import bodyParser from 'body-parser'
import path from 'path'
import { io, metrics, token } from '@middleware'

export function init (app: express.Application) {
  app.use(bodyParser.json())
  app.use(io.log({
    ignoredPatterns: [
      /^\/$/,
      /^\/metrics/,
      /\.js$/,
      /\.css$/
    ]
  }))
  app.use(metrics.measure)
  app.use(token.get)
  app.set('view engine', 'pug')
  app.set('views', path.join(__dirname, '../../views'))
}
