import path from 'path'
import process from 'process'

export const authBaseURL = process.env.AUTH_BASE_URL || ''
export const permissionsBaseURL = process.env.PERMISSIONS_BASE_URL || ''
export const articlesBaseURL = process.env.ARTICLES_BASE_URL || ''

export const PublicDir = path.join(__dirname, '../../public')

export const Port = Number(process.env.PORT || 3000)
export const ShouldCache = process.env.NODE_ENV === 'PRODUCTION'
