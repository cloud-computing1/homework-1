import { authProxy, permissionsProxy, articlesProxy } from '@proxy'
import { metricsService, filesService, usersService, sessionsService, articlesService, permissionsService, cryptoService, csrfService } from '@services'

export function init () {
  const auth = authProxy.getInstance()
  const permissions = permissionsProxy.getInstance()
  const articles = articlesProxy.getInstance()

  const metrics = metricsService.getInstance()
  cryptoService.getInstance()
  filesService.getInstance(metrics)
  usersService.getInstance(auth)
  sessionsService.getInstance(auth)
  csrfService.getInstance(auth, metrics)
  permissionsService.getInstance(permissions)
  articlesService.getInstance(articles)
}
