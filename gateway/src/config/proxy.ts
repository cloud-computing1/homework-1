import { authProxy, permissionsProxy, articlesProxy } from '@proxy'

export function init () {
  authProxy.getInstance()
  permissionsProxy.getInstance()
  articlesProxy.getInstance()
}
