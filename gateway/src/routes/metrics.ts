import express from 'express'
import { metrics } from '@controllers'
import { routing } from '@utils'

export const router = express.Router()

router.get('/', routing.errorWrapper(metrics.get))
