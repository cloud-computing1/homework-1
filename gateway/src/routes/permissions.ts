import express from 'express'
import { permissions } from '@controllers'
import { routing } from '@utils'

export const router = express.Router()

router.get('/', routing.errorWrapper(permissions.getByUserId))
