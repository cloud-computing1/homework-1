import express from 'express'
import { articles } from '@controllers'
import { routing } from '@utils'

export const router = express.Router()

router.get('/', routing.errorWrapper(articles.get))
