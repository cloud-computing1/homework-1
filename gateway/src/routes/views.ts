import express from 'express'
import { views } from '@controllers'
import { routing } from '@utils'

export const router = express.Router()

router.get('/', routing.errorWrapper(views.getIndexPage))
router.get('/login', routing.errorWrapper(views.getLoginPage))
router.get('/flood', routing.errorWrapper(views.getFloodPage))
router.get('/metrics', routing.errorWrapper(views.getMetricsPage))
router.get('/*', routing.errorWrapper(views.getFile))
