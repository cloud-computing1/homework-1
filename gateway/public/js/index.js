/* eslint-disable no-unused-vars */

main()

async function main () {
  const [login, flood, metrics, logout] = document.querySelectorAll('#navbar ul:nth-child(2) li')
  const isLoggedIn = window.localStorage.getItem('token') !== null
  const visibility = ['block', 'none']

  login.style.display = !isLoggedIn ? visibility[0] : visibility[1]
  flood.style.display = isLoggedIn ? visibility[0] : visibility[1]
  metrics.style.display = isLoggedIn ? visibility[0] : visibility[1]
  logout.style.display = isLoggedIn ? visibility[0] : visibility[1]
}

function goToLogin () {
  window.location = '/login'
}

function goToMetrics () {
  window.location = '/metrics'
}

function goToFlood () {
  window.location = '/flood'
}

function goToLogout () {
  window.localStorage.removeItem('token')
  window.location = '/'
}
