/* eslint-disable no-undef */

main()

async function main () {
  const token = window.localStorage.getItem('token')

  if (!token) {
    window.location = '/'
  }

  const api = API()
  const metrics = await api.getMetrics()

  console.log(metrics)

  document.querySelector('#content').innerHTML = httpMetricsCard(metrics.http)
}

function httpMetricsCard (metrics) {
  const card = `<div class="card small-card shadow"><h1>HTTP metrics</h1>$1</div>`
  const showMetric = (number, metric) => `
    <div>
      <h2>Metric number ${number}</h2>
      <p>Started at: ${metric.startedAt}</p>
      <p>Ended at: ${metric.endAt}</p>
      <p>Duration: ${metric.durationMs}ms</p>
      <p>Path: <code>${metric.path}</code><p>
      <p>Params: <code>${JSON.stringify(metric.params)}</code><p>
    </div>
  `

  const elements = metrics.reduce((result, metric, index) => `${result}${showMetric(index, metric)}`, '')

  return card.replace('$1', elements)
}
