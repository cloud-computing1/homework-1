/* eslint-disable no-undef */

main()

async function main () {
  const form = document.getElementById('login')
  const token = window.localStorage.getItem('token')

  if (token) {
    window.location = '/'
  }

  form.onsubmit = onSubmit
}

async function onSubmit (event) {
  event.preventDefault()

  const email = event.target[0].value
  const password = event.target[1].value

  const api = API()
  const response = await api.login(email, password)

  window.localStorage.setItem('token', response.token)
  window.location = '/'
}
