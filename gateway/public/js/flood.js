/* eslint-disable no-undef */

main()

async function main () {
  const form = document.getElementById('flood')
  const token = window.localStorage.getItem('token')

  if (!token) {
    window.location = '/'
  }

  form.onsubmit = onSubmit
}

async function onSubmit (event) {
  event.preventDefault()

  const api = API()
  const instance = timer()
  
  document.querySelector('#content').innerHTML += awaitingCard()

  const [round, result] = await instance.timeIt(async () => {
    const batches = batch(10, () => batch(50, () => api.getArticles()))
    const flatBatch = batches.flat()
  
    await Promise.all(flatBatch)
  })

  document.querySelector('#content').lastElementChild.remove()
  document.querySelector('#content').innerHTML += resultsCard(round, result)
}

function timer () {
  let round = 0;

  return {
    timeIt
  }
  
  async function timeIt (fn) {
    round++

    const then = Date.now()
    await fn()
    const now = Date.now()
    const time = now - then

    console.info(`Time for round ${round}: ${time}ms`)

    return [round, time]
  }
}

function batch (number, fn) {
  const promises = Array(number).fill('').map(() => fn())
  return promises || []
}

function awaitingCard () {
  return `<div class="card small-card shadow"><h1>Awaiting for requests to finish....</p></div>`
}

function resultsCard (round, result) {
  return `<div class="card small-card shadow"><h1>Result number ${round}</h1><p>Time was ${result}ms</p></div>`
}

