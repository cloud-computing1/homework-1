/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */

function API () {
  // const csrfToken = document.querySelector("meta[name='csrf']")
  const accessToken = window.localStorage.getItem('token')

  return {
    login,
    getArticles,
    getMetrics
  }

  async function login (email, password) {
    const url = '/api/sessions'
    const request = await fetch(url, {
      method: 'post',
      headers: {
        'Content-type': 'application/json'
      },
      body: JSON.stringify({ email, password })
    })

    return request.json()
  }

  async function getArticles () {
    const url = '/api/articles'
    const request = await fetch(url, {
      headers: { Authorization: `Bearer ${accessToken}` }
    })

    return request.json()
  }

  async function getMetrics () {
    const url = '/api/metrics'
    const request = await fetch(url, {
      headers: { Authorization: `Bearer ${accessToken}` }
    })

    guard(request)

    return request.json()
  }

  function guard (request) {
    if (request.status === 401) {
      window.localStorage.removeItem('token')
      window.location = '/'
    }
  }
}
