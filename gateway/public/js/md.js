/* eslint-disable no-unused-vars */

function tokenize (article) {
  const templates = [
    ['title', /^# /],
    ['tag', /^!/],
    ['bracket-open', /^\[/],
    ['bracket-close', /^\]/],
    ['para-open', /^\(/],
    ['para-close', /^\)/],
    ['image', /^!\[(.+)\]\((.+)\)/],
    ['newline', /^\n+/],
    ['spacing', /^[ \t]+/],
    ['word', /^([\w\dÀ-ÖØ-öø-įĴ-őŔ-žǍ-ǰǴ-ǵǸ-țȞ-ȟȤ-ȳɃɆ-ɏḀ-ẞƀ-ƓƗ-ƚƝ-ơƤ-ƥƫ-ưƲ-ƶẠ-ỿ'",./\-—_:]+)/]
  ]
  const tokens = []

  let formatedArticle = article.replace(/\\n/g, '\n')
  let previousArticle = formatedArticle

  while (formatedArticle !== '') {
    const token = templates.reduce((acc, [type, regexp]) => {
      if (acc) {
        return acc
      }

      const match = formatedArticle.match(regexp)

      if (match) {
        return [type, match[0]]
      } else {
        return null
      }
    }, null)

    if (!token) {
      throw new Error(formatedArticle)
    }

    tokens.push(token)
    previousArticle = formatedArticle
    formatedArticle = formatedArticle.replace(token[1], '')

    if (previousArticle === formatedArticle) {
      throw new Error('NullToken')
    }
  }

  return tokens
}

function parse (tokens) {
  const templates = [
    [/^tag bracket-open ((word|spacing|tag) )+bracket-close para-open (word )+para-close+/, (result) => result.replace(/!\[(.+)\]\((.+)\)/, '<img src="$2" alt="$1"></img>')],
    [/^bracket-open ((word|spacing|tag) )+bracket-close para-open (word )+para-close+/, (result) => result.replace(/\[(.+)\]\((.+)\)/, '<a href="$2" target="_blank">$1</a>')],
    [/^title ((word|spacing|tag|para-open|para-close|bracket-open|bracket-close) )+newline/, (result) => `<h1>${result.replace(/(# )|\n/g, '')}</h1>`],
    [/^(word|spacing|tag|para-open|para-close|bracket-open|bracket-close)([ ](word|spacing|tag|para-open|para-close|bracket-open|bracket-close))*/, (result) => `<p>${result}</p>`],
    [/^(newline)*/, () => '']
  ]
  const elements = []

  let formattedTokens = tokens.reduce((result, [type]) => `${result} ${type}`, '').trim()
  let previousFormatedTokens = formattedTokens

  while (formattedTokens !== '') {
    const tokenMatch = templates.reduce((acc, [regexp, replacer]) => {
      if (acc) {
        return acc
      }

      const match = formattedTokens.match(regexp)

      if (match) {
        return [match[0], replacer]
      } else {
        return null
      }
    }, null)

    if (!tokenMatch) {
      throw new Error(formattedTokens)
    }

    const tokenMatchLength = tokenMatch[0].split(' ').length
    const match = tokens.slice(0, tokenMatchLength).map(token => token[1]).join('')
    const replacement = tokenMatch[1](match)

    elements.push(replacement)
    previousFormatedTokens = formattedTokens
    formattedTokens = formattedTokens.replace(tokenMatch[0], '').trim()
    tokens.splice(0, tokenMatchLength)

    if (previousFormatedTokens === formattedTokens) {
      throw new Error('NullParse')
    }
  }

  return elements.join('')
}
