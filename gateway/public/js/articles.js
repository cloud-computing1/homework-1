/* eslint-disable no-undef */

main()

async function main () {
  const isLoggedIn = window.localStorage.getItem('token') !== undefined
  const content = document.getElementById('content')

  if (isLoggedIn) {
    const api = API()
    
    const articles = await api.getArticles()
    const tokenizedArticles = articles.map(article => tokenize(article.article))
    const processedArticles = tokenizedArticles.map(tokens => parse(tokens))
  
    processedArticles.forEach(article => (content.innerHTML += articleWrap(article)))
  }

}

function articleWrap (article) {
  const articleTemplate = `
    <article class="card shadow">
      $1
    </article
  `

  return articleTemplate.replace('$1', article)
}
