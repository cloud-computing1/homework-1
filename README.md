# Homework 1

Monorepo with some microservices. Provided by [Mihai Bojescu](https://github.com/MihaiBojescu).

## Building

Enter the microservice dir and run:
```sh
npm run build
```

## Running

This can be done only after building. To run, use:
```sh
npm start 
```

## Debugging

To run in debug mode, use:
```sh
npm run debug
```
The intended way to debug is to attach to the debug process using the inspector provided by node. A sample debug config file can be seen in `.vscode/launch.json`.
