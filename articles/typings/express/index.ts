declare namespace Express {
  interface Request {
    token?: string;
    user?: {
      userId: string,
      email: string
    };
  }
}
