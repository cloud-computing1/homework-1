import express from 'express'
import { database, proc, repositories, server, services } from '@config'
import { articlesRouter } from '@routes'
import { handleError } from '@middleware'

export async function app (): Promise<express.Application> {
  const app = express()

  proc.init()
  await database.init()
  repositories.init()
  services.init()
  server.init(app)

  app.use('/api/articles', articlesRouter)
  app.use(handleError)

  return app
}
