import { ArticlesRepository } from '@repositories/articles'
import { PublicArticle, Singleton, SingletonContainer } from '@types'
import { APIError, singleton } from '@utils'

export interface ArticlesService extends Singleton {
  get: (containing: string, skip: number, take: number) => Promise<PublicArticle[]>
}

const container: SingletonContainer<ArticlesService> = { current: null }

export const getInstance = (
  articlesRepository?: ArticlesRepository
) => singleton.create(container, () => {
  if (!articlesRepository) {
    throw new Error('[Articles Service] Init error')
  } else {
    return constructor(articlesRepository)
  }
})

function constructor (
  articlesRepository: ArticlesRepository
): ArticlesService {
  return {
    get
  }

  async function get (containing: string, skip: number, take: number): Promise<PublicArticle[]> {
    try {
      const articles = await articlesRepository.get(containing, skip, take)
      return articles
    } catch (err) {
      throw new APIError(err)
    }
  }
}
