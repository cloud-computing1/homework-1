import { authProxy } from '@proxy'
import { articlesRepository } from '@repositories'
import { authService, articlesService, cryptoService, metricsService } from '@services'

export function init () {
  const auth = authProxy.getInstance()
  const articlesRepo = articlesRepository.getInstance()

  cryptoService.getInstance()
  metricsService.getInstance()
  authService.getInstance(auth)
  articlesService.getInstance(articlesRepo)
}
