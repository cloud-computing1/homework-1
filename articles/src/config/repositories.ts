import { articlesRepository } from '@repositories'

export function init () {
  articlesRepository.getInstance()
}
