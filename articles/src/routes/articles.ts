import express from 'express'
import { auth, token } from '@middleware'
import { articles } from '@controllers'
import { routing } from '@utils'

export const router = express.Router()

router.get('/', routing.errorWrapper(token.get), routing.errorWrapper(auth.handle), routing.errorWrapper(articles.get))
