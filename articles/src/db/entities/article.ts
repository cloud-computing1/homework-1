import { Column, Entity, PrimaryGeneratedColumn, Unique } from 'typeorm'

@Entity('articles')
@Unique(['id'])
export class Article {
  @PrimaryGeneratedColumn('uuid')
  id!: string

  @Column('uuid')
  userId!: string

  @Column('text')
  article!: string
}
