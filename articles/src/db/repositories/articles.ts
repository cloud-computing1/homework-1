
import { getConnection } from 'typeorm'
import { constants } from '@config'
import { Article } from '@entities'
import { Singleton, SingletonContainer } from '@types'
import { singleton } from '@utils'

export interface ArticlesRepository extends Singleton {
  get: (containing: string, skip: number, take: number) => Promise<Article[]>
}

const container: SingletonContainer<ArticlesRepository> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): ArticlesRepository {
  const articlesRepository = getConnection(constants.Database.name).getRepository(Article)

  return {
    get
  }

  async function get (_: string, skip: number, take: number): Promise<Article[]> {
    const articles = await articlesRepository.find({ skip, take })
    return articles || []
  }
}
