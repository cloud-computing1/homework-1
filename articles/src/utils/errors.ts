export const APIErrorType = {
  auth: { status: 401, data: { message: 'Not signed in' } },
  notFound: { status: 404, data: { message: 'Not found' } },
  internal: { status: 500, data: { message: 'Internal server error' } },
  401: { status: 401, data: { message: 'Not signed in' } },
  404: { status: 404, data: { message: 'Not found' } },
  500: { status: 500, data: { message: 'Internal server error' } }
}

export class APIError extends Error {
  error: Error
  status: number
  data: Record<string, unknown>

  constructor (error: Error) {
    super(error.message)
    this.error = error
    this.status = this.getErrorType()
    this.data = APIErrorType[this.status as keyof typeof APIErrorType].data
  }

  getErrorType (): number {
    return 500
  }

  toString () {
    return `API error: '${this.status}' error at ${this.error.stack}`
  }
}

export const isAPIError = (err: Error | APIError): err is APIError => Object.keys(err).includes('status') && Object.keys(err).includes('data')
