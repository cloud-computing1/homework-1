import express from 'express'
import { AsyncMiddleware } from '@types'

export function errorWrapper (
  asyncMiddleware: AsyncMiddleware | express.Handler
): express.Handler {
  return async (req, res, next): Promise<void> => {
    try {
      await asyncMiddleware(req, res, next)
    } catch (err) {
      next(err)
    }
  }
};
