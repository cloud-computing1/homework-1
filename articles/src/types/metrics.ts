import express from 'express'

export type HTTPMetric = {
  startedAt: string,
  endAt: string,
  durationMs: number,
  path: string,
  params: express.Request['params']
}

export type Metrics = {
  http: HTTPMetric[],
}
