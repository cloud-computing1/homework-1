import perfHooks from 'perf_hooks'
import express from 'express'
import { metricsService } from '@services'

export function measure (req: express.Request, res: express.Response, next: express.NextFunction): void {
  const path = req.path
  const params = req.params
  const then = perfHooks.performance.now()

  res.on('finish', () => {
    const now = perfHooks.performance.now()

    metricsService.getInstance().addHTTPMetric({
      startedAt: new Date(then).toISOString(),
      endAt: new Date(now).toISOString(),
      durationMs: now - then,
      params,
      path
    })
  })

  next()
}
