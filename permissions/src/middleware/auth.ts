import express from 'express'
import { authService } from '@services'
import { APIError } from '@utils'

export async function handle (req: express.Request, _: express.Response, next: express.NextFunction) {
  try {
    const token = req.token || ''
    const session = await authService.getInstance().getByToken(token)
    const user = await authService.getInstance().getUserById(token, session.userId)

    req.user = user

    next()
  } catch (err) {
    throw new APIError(err)
  }
}
