import express from 'express'
import { APIError, APIErrorType, isAPIError } from '@utils'

export function handleError (err: Error | APIError, _1: express.Request, res: express.Response, next: express.NextFunction) {
  if (!err) {
    return next()
  }

  log(err)
  handle(err)

  function log (err: Error | APIError) {
    if (isAPIError(err)) {
      console.error(err.toString())
    } else {
      console.error(`Generic error: '${err.message}' error at ${err.stack}`)
    }
  }

  function handle (err: Error | APIError) {
    if (isAPIError(err)) {
      res.status(err.status).send(err.data)
    } else {
      res.status(APIErrorType.internal.status).send(APIErrorType.internal.data)
    }
  }
}
