export type Singleton = {
  [key: string]: unknown
}

export type SingletonContainer<TSingleton> = {
  current: TSingleton | null
}
