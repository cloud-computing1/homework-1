export type PublicPermissions = {
  userId: string,
  permissions: {
    [scope: string]: string[]
  }
}
