import 'reflect-metadata'

import { app } from './app'
import { constants } from '@config'

app().then(app => app.listen(constants.Port, () => {
  console.log('App is up on port', constants.Port)
}))
