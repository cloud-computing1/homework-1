import express from 'express'
import { database, proc, proxy, repositories, server, services } from '@config'
import { permissionsRouter } from '@routes'

export async function app (): Promise<express.Application> {
  const app = express()

  proc.init()
  await database.init()
  repositories.init()
  proxy.init()
  services.init()
  server.init(app)

  app.use('/api/permissions', permissionsRouter)

  return app
}
