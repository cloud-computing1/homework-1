import { Metrics, HTTPMetric, Singleton, SingletonContainer } from '@types'
import { singleton } from '@utils'

export interface MetricsService extends Singleton {
  get: () => Metrics
  addHTTPMetric: (metric: HTTPMetric) => void
}

const container: SingletonContainer<MetricsService> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): MetricsService {
  const metrics: Metrics = {
    http: []
  }

  return {
    get,
    addHTTPMetric
  }

  function get (): Metrics {
    return metrics
  }

  function addHTTPMetric (metric: HTTPMetric) {
    metrics.http.push(metric)
  }
}
