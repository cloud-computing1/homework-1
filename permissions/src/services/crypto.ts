import util from 'util'
import crypto from 'crypto'
import { Singleton, SingletonContainer } from '@types'
import { APIError, singleton } from '@utils'

export interface CryptoService extends Singleton {
  randomStringHex: (size: number) => Promise<string>
  randomStringBase64: (size: number) => Promise<string>
  hash: (text: string) => string
}

const container: SingletonContainer<CryptoService> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): CryptoService {
  const hashAlgorithm = 'whirlpool'
  const randomBytes = util.promisify(crypto.randomBytes)

  return {
    randomStringHex,
    randomStringBase64,
    hash
  }

  async function randomStringHex (size: number): Promise<string> {
    try {
      const bytes = await randomBytes(size)
      const string = bytes.toString('hex')

      return string
    } catch (err) {
      throw new APIError(err)
    }
  }

  async function randomStringBase64 (size: number): Promise<string> {
    try {
      const bytes = await randomBytes(size)
      const string = bytes.toString('base64')

      return string
    } catch (err) {
      throw new APIError(err)
    }
  }

  function hash (text: string): string {
    try {
      return crypto
        .createHash(hashAlgorithm)
        .update(text)
        .digest('base64')
    } catch (err) {
      throw new APIError(err)
    }
  }
}
