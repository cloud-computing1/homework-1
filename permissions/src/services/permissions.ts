import { PermissionsRepository } from '@repositories/permissions'
import { PublicPermissions, Singleton, SingletonContainer } from '@types'
import { singleton } from '@utils'

export interface PermissionsService extends Singleton {
  getByUserId: (userId: string) => Promise<PublicPermissions>
}

const container: SingletonContainer<PermissionsService> = { current: null }

export const getInstance = (
  permissionsRepository?: PermissionsRepository
) => singleton.create(container, () => {
  if (!permissionsRepository) {
    throw new Error('[Permissions service] Init error')
  } else {
    return constructor(permissionsRepository)
  }
})

function constructor (
  permissionsRepository: PermissionsRepository
): PermissionsService {
  return {
    getByUserId
  }

  async function getByUserId (userId: string): Promise<PublicPermissions> {
    const permissions = await permissionsRepository.getByUserId(userId)

    const publicPermissions = permissions.reduce(
      (result, permission) => ({
        userId,
        permissions: {
          ...result.permissions,
          [permission.scope]: [
            ...(result.permissions?.[permission.scope] ?? []),
            permission.permission
          ]
        }
      }),
      { userId, permissions: {} } as PublicPermissions
    )

    return publicPermissions
  }
}
