import { AuthProxy } from '@proxy/auth'
import { APIError, singleton } from '@utils'
import { PublicSession, PublicUser, Singleton, SingletonContainer } from '@types'

export interface SessionsService extends Singleton {
  getByToken: (token: string) => Promise<PublicSession>
  getUserById: (token: string, userId: string) => Promise<PublicUser>
}

const container: SingletonContainer<SessionsService> = { current: null }

export const getInstance = (
  authProxy?: AuthProxy
) => singleton.create(container, () => {
  if (!authProxy) {
    throw new Error('[Auth service] Init error')
  } else {
    return constructor(authProxy)
  }
})

function constructor (
  authProxy: AuthProxy
): SessionsService {
  return {
    getByToken,
    getUserById
  }

  async function getByToken (token: string): Promise<PublicSession> {
    try {
      const session = await authProxy.getSessionByToken(token)
      return session
    } catch (err) {
      throw new APIError(err)
    }
  }

  async function getUserById (token: string, userId: string): Promise<PublicUser> {
    try {
      const user = await authProxy.getUserById(token, userId)
      return user
    } catch (err) {
      throw new APIError(err)
    }
  }
}
