import express from 'express'
import { permissionsService } from '@services'

export async function get (req: express.Request, res: express.Response) {
  const userId = Array.isArray(req.query.userId)
    ? req.query.userId[0] ?? ''
    : req.query.userId ?? ''

  const result = await permissionsService.getInstance().getByUserId(userId as string)

  res.status(200).send(result)
}
