
import { getConnection } from 'typeorm'
import { constants } from '@config'
import { Permission } from '@entities'
import { Singleton, SingletonContainer } from '@types'
import { singleton } from '@utils'

export interface PermissionsRepository extends Singleton {
  getByUserId: (userId: string) => Promise<Permission[]>
}

const container: SingletonContainer<PermissionsRepository> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): PermissionsRepository {
  const permissionsRepository = getConnection(constants.Database.name).getRepository(Permission)

  return {
    getByUserId
  }

  async function getByUserId (userId: string): Promise<Permission[]> {
    const permissions = await permissionsRepository.find({ userId })
    return permissions
  }
}
