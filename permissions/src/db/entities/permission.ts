import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm'

@Entity('permissions')
export class Permission {
  @PrimaryGeneratedColumn('increment')
  id!: string

  @Column({ name: 'userId', type: 'uuid' })
  userId!: string

  @Column()
  scope!: string

  @Column()
  permission!: string
}
