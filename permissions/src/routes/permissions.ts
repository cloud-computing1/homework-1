import express from 'express'
import { auth, token } from '@middleware'
import { permissions } from '@controllers'

export const router = express.Router()

router.get('/', token.get, auth.handle, permissions.get)
