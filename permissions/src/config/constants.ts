import process from 'process'
import { Permission } from '@entities'
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions'

export const authBaseURL = process.env.AUTH_BASE_URL || ''
export const Port = Number(process.env.PORT || 3002)

export const Database: PostgresConnectionOptions = {
  name: 'postgres',
  type: 'postgres',
  host: process.env.DATABASE_HOST,
  port: Number(process.env.DATABASE_PORT || 5432),
  username: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
  database: 'hw1-permissions',
  entities: [
    Permission
  ]
}
