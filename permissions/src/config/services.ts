import { authProxy } from '@proxy'
import { permissionsRepository } from '@repositories'
import { authService, cryptoService, metricsService, permissionsService } from '@services'

export function init () {
  const auth = authProxy.getInstance()
  const permissionsRepo = permissionsRepository.getInstance()

  cryptoService.getInstance()
  metricsService.getInstance()
  authService.getInstance(auth)
  permissionsService.getInstance(permissionsRepo)
}
