import { permissionsRepository } from '@repositories'

export function init () {
  permissionsRepository.getInstance()
}
