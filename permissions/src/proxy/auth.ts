import axios from 'axios'
import { constants } from '@config'
import { singleton } from '@utils'
import { PublicSession, PublicUser, Singleton, SingletonContainer } from '@types'

export interface AuthProxy extends Singleton {
  getSessionByToken: (token: string) => Promise<PublicSession>
  getUserById: (token: string, userId: string) => Promise<PublicUser>
}

const container: SingletonContainer<AuthProxy> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): AuthProxy {
  const client = axios.create({
    baseURL: constants.authBaseURL
  })

  return {
    getSessionByToken,
    getUserById
  }

  async function getSessionByToken (token: string): Promise<PublicSession> {
    const response = await client.get<PublicSession>(`/api/sessions/${token}`)
    return response.data
  }

  async function getUserById (token: string, userId: string): Promise<PublicUser> {
    const response = await client.get<PublicUser>(`/api/users/${userId}`, {
      headers: { Authorization: `Bearer ${token}` }
    })
    return response.data
  }
}
