import express from 'express'
import { cryptoService } from '@services'

interface Options {
  ignoredPatterns?: RegExp[],
}

export function log (options: Options) {
  return async (req: express.Request, res: express.Response, next: express.NextFunction): Promise<void> => {
    if (options.ignoredPatterns?.find(regex => regex.test(req.path))) {
      return next()
    }

    const id = await cryptoService.getInstance().randomStringBase64(24)
    const originalStatus = res.status
    const originalSend = res.send

    let sentCode = 0
    let sentBody = {}

    console.log(`[REQUEST  id: ${id}] Got a request on path ${req.path} with body ${JSON.stringify(req.body, null, 4)}`)

    res.status = (code: number) => {
      sentCode = code
      return originalStatus.call(res, code)
    }
    res.send = (body?: any) => {
      sentBody = body
      return originalSend.call(res, body)
    }

    res.on('finish', () => {
      console.log(`[RESPONSE id: ${id}] Responded with ${sentCode} and body ${JSON.stringify(sentBody, null, 4)}`)
    })

    next()
  }
}
