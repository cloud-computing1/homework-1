export type PublicCSRFToken = {
  token: string
}

export type PublicCSRFTokenValidity = {
  valid: boolean
}
