export type PublicSession = {
  userId: string,
  token: string
}
