import express from 'express'

export type HTTPMetric = {
  startedAt: string,
  endAt: string,
  durationMs: number,
  path: string,
  params: express.Request['params']
}

export type CSRFMetrics = {
  tokens: number,
  successfulRequests: number,
  failedRequests: number
}

export type FileMetrics = {
  cached: Record<string, string>
}

export type UsersMetrics = {
  count: number
}

export type Metrics = {
  http: HTTPMetric[],
  csrf: CSRFMetrics,
  files: FileMetrics,
  users: UsersMetrics,
}
