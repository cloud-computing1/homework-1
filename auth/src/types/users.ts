export type PublicUser = {
  userId: string,
  email: string
}

export type Admins = {
  [userId: string]: boolean
}
