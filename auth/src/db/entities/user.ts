import { Column, Entity, PrimaryGeneratedColumn, Unique } from 'typeorm'

@Entity('users')
@Unique(['userId', 'email'])
export class User {
  @PrimaryGeneratedColumn('uuid')
  userId!: string

  @Column()
  email!: string

  @Column()
  password!: string

  @Column('bool')
  isDisabled!: boolean
}
