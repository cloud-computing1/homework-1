import { Column, Entity, JoinColumn, ManyToOne, PrimaryGeneratedColumn, Unique } from 'typeorm'
import { User } from './user'

@Entity('sessions')
@Unique(['id', 'token'])
export class Session {
  @PrimaryGeneratedColumn('increment')
  id!: string

  @ManyToOne(() => User, user => user.userId, { onUpdate: 'CASCADE', onDelete: 'CASCADE' })
  @JoinColumn({ name: 'userId' })
  @Column()
  userId!: string

  @Column('char', { length: 64 })
  token!: string
}
