
import { getConnection } from 'typeorm'
import { constants } from '@config'
import { User } from '@entities'
import { Singleton, SingletonContainer } from '@types'
import { singleton } from '@utils'

export interface UsersRepository extends Singleton {
  create: (email: string, password: string) => Promise<User>
  getByUserId: (userId: string) => Promise<User | undefined>
  getByEmailAndPassword: (email: string, password: string) => Promise<User | undefined>
  removeByUserId: (userId: string) => Promise<void>
}

const container: SingletonContainer<UsersRepository> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): UsersRepository {
  const usersRepository = getConnection(constants.Database.name).getRepository(User)

  return {
    create,
    getByUserId,
    getByEmailAndPassword,
    removeByUserId
  }

  async function create (email: string, password: string): Promise<User> {
    const user = new User()

    user.email = email
    user.password = password

    return usersRepository.save(user)
  }

  async function getByUserId (userId: string): Promise<User | undefined> {
    const user = await usersRepository.findOne({ userId })
    return user
  }

  async function getByEmailAndPassword (email: string, password: string): Promise<User | undefined> {
    const user = await usersRepository.findOne({ email, password })
    return user
  }

  async function removeByUserId (userId: string): Promise<void> {
    await usersRepository.update(userId, { isDisabled: true })
  }
}
