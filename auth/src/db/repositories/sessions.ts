
import { getConnection } from 'typeorm'
import { constants } from '@config'
import { Session } from '@entities'
import { Singleton, SingletonContainer } from '@types'
import { singleton } from '@utils'

export interface SessionsRepository extends Singleton {
  create: (userId: string, token: string) => Promise<Session>
  getByToken: (token: string) => Promise<Session | undefined>
  removeByToken: (token: string) => Promise<void>
  removeAllForUser: (userId: string) => Promise<void>
}

const container: SingletonContainer<SessionsRepository> = { current: null }

export const getInstance = () => singleton.create(container, constructor)

function constructor (): SessionsRepository {
  const sessionsRepository = getConnection(constants.Database.name).getRepository(Session)

  return {
    create,
    getByToken,
    removeByToken,
    removeAllForUser
  }

  async function create (userId: string, token: string): Promise<Session> {
    const session = new Session()

    session.userId = userId
    session.token = token

    return sessionsRepository.save(session)
  }

  async function getByToken (token: string): Promise<Session | undefined> {
    const session = await sessionsRepository.findOne({ token })
    return session
  }

  async function removeByToken (token: string): Promise<void> {
    await sessionsRepository.delete({ token })
  }

  async function removeAllForUser (userId: string): Promise<void> {
    await sessionsRepository.delete({ userId })
  }
}
