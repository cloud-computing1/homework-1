import express from 'express'
import { users } from '@controllers'
import { routing } from '@utils'

export const router = express.Router()

router.post('/', routing.errorWrapper(users.create))
router.get('/:userId', routing.errorWrapper(users.getById))
