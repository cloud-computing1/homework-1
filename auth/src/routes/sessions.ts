import express from 'express'
import { sessions } from '@controllers'
import { routing } from '@utils'

export const router = express.Router()

router.post('/', routing.errorWrapper(sessions.create))
router.get('/:sessionId', routing.errorWrapper(sessions.getByToken))
