import express from 'express'
import { csrf } from '@controllers'
import { routing } from '@utils'

export const router = express.Router()

router.get('/', routing.errorWrapper(csrf.get))
router.get('/:token', routing.errorWrapper(csrf.check))
