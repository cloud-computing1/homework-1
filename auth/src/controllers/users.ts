import express from 'express'
import { usersService } from '@services'

export async function getById (req: express.Request, res: express.Response) {
  const userId = req.params.userId || ''

  const user = await usersService.getInstance().getByUserId(userId)

  res.status(200).send(user)
}

export async function create (req: express.Request, res: express.Response) {
  const email = req.body.email || ''
  const password = req.body.password || ''

  const user = await usersService.getInstance().create(email, password)

  res.status(200).send(user)
}
