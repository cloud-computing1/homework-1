import express from 'express'
import { csrfService } from '@services'

export async function get (_: express.Request, res: express.Response) {
  const token = await csrfService.getInstance().get()

  res.status(200).send(token)
}

export async function check (req: express.Request, res: express.Response) {
  const token = Array.isArray(req.params.token)
    ? req.params.token[0] as string || ''
    : req.params.token as string || ''

  const validity = await csrfService.getInstance().check(token)

  res.status(200).send(validity)
}
