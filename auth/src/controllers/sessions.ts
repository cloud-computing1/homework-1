import express from 'express'
import { usersService, sessionsService } from '@services'
import { AuthError } from '@utils'

export async function getByToken (req: express.Request, res: express.Response) {
  const token = req.params.sessionId || ''

  const session = await sessionsService.getInstance().getByToken(token)

  if (session) {
    res.status(200).send(session)
  } else {
    throw new AuthError()
  }
}

export async function create (req: express.Request, res: express.Response) {
  const email = req.body.email || ''
  const password = req.body.password || ''

  const user = await usersService.getInstance().getByEmailAndPassword(email, password)

  if (user) {
    const sessions = await sessionsService.getInstance().create(user.userId)

    res.status(200).send(sessions)
  } else {
    throw new AuthError()
  }
}
