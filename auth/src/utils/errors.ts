export const APIErrorType = {
  auth: { status: 401, data: { message: 'Not signed in' } },
  notFound: { status: 404, data: { message: 'Not found' } },
  internal: { status: 500, data: { message: 'Internal server error' } },
  401: { status: 401, data: { message: 'Not signed in' } },
  404: { status: 404, data: { message: 'Not found' } },
  500: { status: 500, data: { message: 'Internal server error' } }
}

export class APIError extends Error {
  error: Error
  status: number
  data: Record<string, unknown>

  constructor (error: Error) {
    super(error.message)
    this.error = error
    this.status = 500
    this.data = APIErrorType[500].data
  }

  toString () {
    return `Internal API error: '${this.status}' error at ${this.error.stack}`
  }
}

export class AuthError extends Error {
  status: number
  data: Record<string, unknown>

  constructor () {
    super()
    this.status = 401
    this.data = APIErrorType[401].data
  }

  toString () {
    return `Auth error: '${this.status}' at ${this.stack}`
  }
}

export const isAPIError = (err: Error | APIError): err is APIError => Object.keys(err).includes('status') && Object.keys(err).includes('data')
export const isAuthError = (err: Error | APIError): err is APIError => !Object.keys(err).includes('error') && Object.keys(err).includes('status') && Object.keys(err).includes('data')
