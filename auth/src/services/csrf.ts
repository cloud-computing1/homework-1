import { CryptoService } from '@services/crypto'
import { MetricsService } from '@services/metrics'
import { constants } from '@config'
import { CSRFMetrics, PublicCSRFToken, PublicCSRFTokenValidity, Singleton, SingletonContainer } from '@types'
import { APIError, singleton } from '@utils'

export interface CSRFService extends Singleton {
  get: () => Promise<PublicCSRFToken>
  check: (token: string) => PublicCSRFTokenValidity
}

const container: SingletonContainer<CSRFService> = { current: null }

export const getInstance = (
  cryptoService?: CryptoService,
  metricsService?: MetricsService
) => singleton.create(container, () => {
  if (!metricsService || !cryptoService) {
    throw new Error('[CSRF Service] Init error')
  } else {
    return constructor(cryptoService, metricsService)
  }
})

function constructor (
  cryptoService: CryptoService,
  metricsService: MetricsService
): CSRFService {
  const metrics: CSRFMetrics = { tokens: 0, successfulRequests: 0, failedRequests: 0 }
  let cache: Record<string, boolean> = {}

  manage()

  return {
    get,
    check
  }

  function manage () {
    setInterval(() => {
      cache = {}
      metrics.tokens = 0
      updateMetrics()
    }, constants.ExpireTime)
  }

  async function get (): Promise<PublicCSRFToken> {
    try {
      const token = await cryptoService.randomStringHex(24)

      cache[token] = true

      updateMetrics()

      return { token }
    } catch (err) {
      throw new APIError(err)
    }
  }

  function check (token: string): PublicCSRFTokenValidity {
    try {
      const success = cache[token] === true

      if (success) {
        metrics.successfulRequests++
      } else {
        metrics.failedRequests++
      }

      updateMetrics()

      return { valid: cache[token] === true }
    } catch (err) {
      throw new APIError(err)
    }
  }

  function updateMetrics () {
    metricsService.setCSRFMetric(metrics)
  }
}
