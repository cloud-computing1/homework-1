import { UsersRepository } from '@repositories/users'
import { CryptoService } from '@services/crypto'
import { PublicUser, Singleton, SingletonContainer } from '@types'
import { APIError, singleton } from '@utils'

export interface UsersService extends Singleton {
  getByUserId: (userId: string) => Promise<PublicUser | null>
  getByEmailAndPassword: (email: string, password: string) => Promise<PublicUser | null>
  create: (email: string, password: string) => Promise<PublicUser>
}

const container: SingletonContainer<UsersService> = { current: null }

export const getInstance = (
  usersRepository?: UsersRepository,
  cryptoService?: CryptoService
) => singleton.create(container, () => {
  if (!usersRepository || !cryptoService) {
    throw new Error('[Users service] Init error')
  } else {
    return constructor(usersRepository, cryptoService)
  }
})

function constructor (
  usersRepository: UsersRepository,
  cryptoService: CryptoService
): UsersService {
  return {
    create,
    getByUserId,
    getByEmailAndPassword
  }

  async function getByUserId (userId: string): Promise<PublicUser | null> {
    try {
      const user = await usersRepository.getByUserId(userId)

      if (!user || user.isDisabled) {
        return null
      }

      const publicUser: PublicUser = {
        userId: user.userId,
        email: user.email
      }

      return publicUser
    } catch (err) {
      throw new APIError(err)
    }
  }

  async function getByEmailAndPassword (email: string, password: string): Promise<PublicUser | null> {
    try {
      const hashedPassword = cryptoService.hash(password)
      const user = await usersRepository.getByEmailAndPassword(email, hashedPassword)

      if (!user || user.isDisabled) {
        return null
      }

      const publicUser: PublicUser = {
        userId: user.userId,
        email: user.email
      }

      return publicUser
    } catch (err) {
      throw new APIError(err)
    }
  }

  async function create (email: string, password: string): Promise<PublicUser> {
    try {
      const hashedPassword = cryptoService.hash(password)
      const user = await usersRepository.create(email, hashedPassword)

      const publicUser: PublicUser = {
        userId: user.userId,
        email: user.email
      }

      return publicUser
    } catch (err) {
      throw new APIError(err)
    }
  }
}
