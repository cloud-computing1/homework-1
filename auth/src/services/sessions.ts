import { SessionsRepository } from '@repositories/sessions'
import { PublicSession, Singleton, SingletonContainer } from '@types'
import { APIError, singleton } from '@utils'
import { CryptoService } from '@services/crypto'

export interface SessionsService extends Singleton {
  create: (userId: string) => Promise<PublicSession>
  getByToken: (token: string) => Promise<PublicSession | null>
}

const container: SingletonContainer<SessionsService> = { current: null }

export const getInstance = (
  sessionsRepository?: SessionsRepository,
  cryptoService?: CryptoService
) => singleton.create(container, () => {
  if (!sessionsRepository || !cryptoService) {
    throw new Error('[Users service] Init error')
  } else {
    return constructor(sessionsRepository, cryptoService)
  }
})

function constructor (
  sessionsRepository: SessionsRepository,
  cryptoService: CryptoService
): SessionsService {
  return {
    create,
    getByToken
  }

  async function create (userId: string): Promise<PublicSession> {
    const token = await cryptoService.randomStringHex(24)

    await sessionsRepository.create(userId, token)

    return { userId, token }
  }

  async function getByToken (token: string): Promise<PublicSession | null> {
    try {
      const session = await sessionsRepository.getByToken(token)

      if (!session) {
        return null
      }

      return { userId: session.userId, token }
    } catch (err) {
      throw new APIError(err)
    }
  }
}
