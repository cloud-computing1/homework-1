import express from 'express'
import bodyParser from 'body-parser'
import { io, metrics } from '@middleware'

export function init (app: express.Application) {
  app.use(bodyParser.json())
  app.use(io.log({
    ignoredPatterns: [
      /^\/$/,
      /^\/metrics/,
      /\.js$/,
      /\.css$/
    ]
  }))
  app.use(metrics.measure)
}
