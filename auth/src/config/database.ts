import { createConnection } from 'typeorm'
import { constants } from '@config'

export async function init () {
  try {
    const connection = await createConnection(constants.Database)

    await connection.synchronize()
  } catch (err) {
    console.error(err)
    throw new Error('[Database] Could not create database connection')
  }
}
