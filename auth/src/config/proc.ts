import process from 'process'

export function init () {
  process.on('uncaughtException', (error) => console.error(error))
  process.on('unhandledRejection', (error) => console.error(error))
}
