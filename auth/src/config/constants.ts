import process from 'process'
import { Session, User } from '@entities'
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions'

export const ExpireTime = 15 * 60 * 1000
export const Port = Number(process.env.PORT || 3001)

export const Database: PostgresConnectionOptions = {
  name: 'postgres',
  type: 'postgres',
  host: process.env.DATABASE_HOST,
  port: Number(process.env.DATABASE_PORT || 5432),
  username: process.env.DATABASE_USERNAME,
  password: process.env.DATABASE_PASSWORD,
  database: 'hw1-auth',
  entities: [
    User,
    Session
  ]
}
