import { sessionsRepository, usersRepository } from '@repositories'
import { cryptoService, csrfService, metricsService, sessionsService, usersService } from '@services'

export function init () {
  const usersRepo = usersRepository.getInstance()
  const sessionsRepo = sessionsRepository.getInstance()

  const crypto = cryptoService.getInstance()
  const metrics = metricsService.getInstance()
  csrfService.getInstance(crypto, metrics)
  usersService.getInstance(usersRepo, crypto)
  sessionsService.getInstance(sessionsRepo, crypto)
}
